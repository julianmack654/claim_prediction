import pandas as pd
import requests
import sys
from io import BytesIO
import json
from DB_info import SQL_ARG
from sqlalchemy import create_engine, sql
from validation import validate_list_company_names, remove_company_stopwords
from features import numeric_continuous_variables, non_zero_int_variables, bool_variables, date_variables, county_mapping, new_numeric_features
from PCMdb import PCMdb as db
import pandas as pd
import numpy as np
import time
import datetime
from datetime import timedelta
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from imblearn.over_sampling import RandomOverSampler
from imblearn.over_sampling import SMOTE
from imblearn.over_sampling import ADASYN
from treeinterpreter import treeinterpreter as ti
from sklearn.metrics import cohen_kappa_score


PCMdb = "QBE_policies_data"
PCM_comp_numbers = "QBE_company_numbers"
TABLE = PCMdb
QBE_2017_YEAR_END = datetime.datetime(2017, 12, 30)
DAYS_YEAR = 365
YEARS = ["yr1", "yr0"]
TEMP_SAVE1 = "temp/df1.csv"
TEMP_SAVE2 = "temp/df2.csv"

"""
80-20 split of data into train and test
X_train, y_train
X_test, y_test
"""
#Processing data
REPLACE_INF = 10
NUMBER_BINS_NUMERICAL = 9#+1 for NaN
BIN_INTERVALS_EMPLOYEEE = pd.IntervalIndex([
    pd.Interval(0, 0),
    pd.Interval(0, 1),
    pd.Interval(1, 5),
    pd.Interval(5, 10),
    pd.Interval(15, 50),
    pd.Interval(50, 250),
    pd.Interval(250, 1000),
    pd.Interval(1000, 2500),
    ])
BIN_INTERVALS_ENTITY_OFFICERS = pd.IntervalIndex([
    pd.Interval(0, 0),
    pd.Interval(0, 1),
    pd.Interval(1, 3),
    pd.Interval(3, 7),
    pd.Interval(7, 15),
    pd.Interval(15, 90),
    ])
BIN_INTERVALS_MORTGAGES = pd.IntervalIndex([
    pd.Interval(0, 0),
    pd.Interval(0, 1),
    pd.Interval(1, 3),
    pd.Interval(3, 7),
    pd.Interval(7, 30),
    pd.Interval(30, 150),
    ])
BIN_INTERVALS_SIC = pd.IntervalIndex([
    pd.Interval(0, 1),
    pd.Interval(1, 2),
    pd.Interval(2, 3),
    pd.Interval(3, 4),
    ])


#Random Forest model parameters
n_estimators=300
min_samples_split=3
criterion="gini"
#criterion="mse"

n_estimators_options = [3**x for x in range(5)]
criteria = ["entropy", "gini"]
min_samples_splits = [2**2 for x in range(4)]


def prepare_data(model="ratio", numerical_bins=True, save_data=False):
    engine = create_engine(SQL_ARG)
    with engine.connect() as conn:
        query = """SELECT *
                    FROM companies."QBE_policies_data_w_accounts_2yrs";"""
        df_original = pd.read_sql(query, conn)

    df = remove_irrelevant_features(df_original)
    df = add_domain_specific_features(df)
    df = save_temp_data(df, save_data, path=TEMP_SAVE1)
    df = preprocess_features(df, numerical_bins)
    df = save_temp_data(df, save_data, path=TEMP_SAVE2)

    #df = scale(df) #- feature scaling. But am putting numerical values in bins
    #print(df)
    col_remove_y = ["Claims", "Claim Count", "Banding"]
    X = df.drop(labels=col_remove_y, axis=1)
    if model == "ratio":
        y = df["Claims"] / df["GWP"] > 0.5
        y = parse_claim_GWP(y) #remove NaN and inf
    elif model == "claim":
        y = df["Claims"] > 0
    else:
        print("Invalid model")
        sys.exit()
    return X, y, df_original

def parse_claim_GWP(y):
    y.rename("Claim/Cost ratio", inplace=True)
    y.fillna(0, inplace=True)
    REPLACE_INF = 10
    y.replace(np.inf, REPLACE_INF, inplace=True)
    return y

def run(X, y, random_state=42, save_features=None, csv_path="results/randomForest_important_features_.csv"):
    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=random_state)

    X_resampled, y_resampled = RandomOverSampler(random_state=9).fit_sample(X_train, y_train)    #random oversampling
    #X_resampled, y_resampled = SMOTE().fit_sample(X_train, y_train) #SMOTE oversampling
    #X_resampled, y_resampled = ADASYN().fit_sample(X_train, y_train) #SMOTE oversampling
    X_train = pd.DataFrame(X_resampled)
    y_train = pd.Series(y_resampled)
    X_train.columns = X.columns

    weights = create_weightings(y_train.copy())
    #weights = weights.iloc[::-1]
    #weights = None
    clf, features_df = run_random_forest(X_train, X_test, y_train, y_test, weights=weights, return_features=True)
    y_pred = clf.predict(X_test)
    if save_features:
        features_df.to_csv(csv_path)
    return clf, X_train, X_test, y_train, y_test, y_pred

    """Not run: """

    relevant_features = features_df.head(200).keys()
    name = "Just with relevant features"
    clf = run_random_forest(X_train[relevant_features], X_test[relevant_features], y_train, y_test, weights=weights, message=name)

    pred = clf.predict(X[relevant_features])
    k_score = cohen_kappa_score(pred, y)

    print("Original data (full set):")
    print("Kappa score: {:.2f}%".format(k_score))
    print(confusion_matrix(y, pred))
    print(classification_report(y, pred))


def run_random_forest(X_train, X_test, y_train, y_test, weights=None, return_features = False, message = None):
    clf = RandomForestClassifier(n_estimators=n_estimators, min_samples_split=min_samples_split, criterion=criterion, n_jobs=-1)

    clf.fit(X_train, y_train, sample_weight=weights)
    pred = clf.predict(X_test)
    k_score = cohen_kappa_score(pred, y_test)
    print()
    print()
    if message:
        print(message)
    print('Accuracy on training set: {:.2f}%'
         .format(clf.score(X_train, y_train)))
    print('Accuracy on test set: {:.2f}%'
         .format(clf.score(X_test, y_test)))
    print("Kappa score: {:.2f}%".format( k_score))
    print(confusion_matrix(y_test, pred))
    print(classification_report(y_test, pred))
    if return_features:
        features_df = pd.Series(clf.feature_importances_, index=X_train.keys())
        features_df = features_df.sort_values(ascending=False)
        return clf, features_df
    print()
    print()
    return clf

def create_weightings(y):
    counts = y.value_counts()
    weights = {}
    base = 10
    for key in counts.keys():
        if key == True:
            multiplier = base*100
        else:
            multiplier = base
        weights[key] = 1/counts[key]*multiplier
    y.replace(weights, inplace=True)
    #print(pd.DataFrame([z,y]).transpose())
    return y



def preprocess_features(df, numerical_bins):
    for variable in bool_variables:
        for column in df.keys():
            if variable in column:
                df[column].loc[pd.isnull(df[column])] = False
    #df = preprocess_numeric_features_replace_missing(df)
    df = preprocess_date_features(df)
    df = preprocess_numeric_features(df, numerical_bins)
    le=LabelEncoder()
    ohe=OneHotEncoder(sparse=False)
    for col in df.columns.values:
        # Encoding only categorical variables
        if df[col].dtypes=='object':
            le.fit(df[col])
            new_features = le.transform(df[col])
            new_features = new_features.reshape(-1,1)
            ohe.fit(new_features)
            temp = ohe.transform(new_features)
            temp=pd.DataFrame(temp, columns=[(col+"_"+str(i)) for i in df[col].value_counts().index])
            temp=temp.set_index(df.index.values)
            df = pd.concat([df,temp], axis=1)
            df = df.drop(labels=col, axis=1)
    return df


def preprocess_date_features(df):
    def fn(x):
        if x == np.nan or x == None:
            return x
        else:
            try:
                final = (QBE_2017_YEAR_END - x).days
            except TypeError:
                y = datetime.datetime(x.year, x.month, x.day)
                final = (QBE_2017_YEAR_END - y).days
            return final
    #df["DissolutionDate"] = datetime.datetime(df["DissolutionDate"])
    date_columns = get_col_names_containing(date_variables, df)
    for column in date_columns:
        df[column] = df[column].apply(fn)
    return df


def preprocess_numeric_features(df, numerical_bins=True):

    num_var_col = get_col_names_containing(numeric_continuous_variables, df)
    date_var_col = get_col_names_containing(date_variables, df)
    int_var_col = get_col_names_containing(non_zero_int_variables, df)
    new_cols = get_col_names_containing(new_numeric_features, df)
    num_var_col.extend(date_var_col)
    num_var_col.extend(int_var_col)
    num_var_col.extend(new_cols)
    num_var_col = list(set(num_var_col))
    df = update_df_column_type(df, num_var_col, "float64")
    if numerical_bins:
        df = put_numerical_in_bins(df, num_var_col)
        df[num_var_col] = df[num_var_col].fillna(pd.Interval(-0.1, 0.1)) #for NaN
    else:
        df[num_var_col] = df[num_var_col].fillna(0)  #for NaN

    return df

def put_numerical_in_bins(df, num_var_col):
    for column in num_var_col:
        if "Mortgages" in column:
            df[column] = pd.cut(df[column], bins=BIN_INTERVALS_MORTGAGES)
        elif "AverageNumberEmployeesDuringPeriod" in column:
            df[column] = pd.cut(df[column], bins=BIN_INTERVALS_EMPLOYEEE)
        elif "NumberEntityOfficer" in column:
            df[column] = pd.cut(df[column], bins=BIN_INTERVALS_ENTITY_OFFICERS)
        elif "NumberSicCodes" in column:
            df[column] = pd.cut(df[column], bins=BIN_INTERVALS_SIC)
        else:
            df[column] = pd.qcut(df[column], NUMBER_BINS_NUMERICAL, duplicates="drop")
    df = update_df_column_type(df, num_var_col, "object")

    return df


def update_df_column_type(df, columns, new_type):
    for variable in columns:
        for column in df.keys():
            if variable in column:
                df[column] = df[column].astype(new_type)
    return df

def get_col_names_containing(variables_list, df):
    columns_final = []
    for variable in variables_list:
        min_samples_splits = [2**x for x in range(1,4)]
        columns_final.extend([x for x in df.keys() if variable in x])
    return columns_final

def make_employee_positive(df):
    def fn(x):
        if x == np.nan or x == None:
            return x
        else:
            return abs(float(x))
    columns = get_col_names_containing(non_zero_int_variables, df)
    for column in columns:
        df[column] = df[column].apply(fn)
    return df
###################
#Add domain specific features
def add_domain_specific_features(df):
    def ratio_values(df, numerator, denominator, name):
        return df[numerator] / df[denominator]
    df = add_TotalAssetsLessLiabilities_calculated(df)
    df = add_TotalLiabilities(df)
    df = add_NumberSicCodes(df)
    df = update_df_column_type(df, non_zero_int_variables, "float64")
    df = add_mortgage_ratio_outstanding(df)
    df = add_ratio_employee_officers(df, ratio_values)
    df = add_ratio_provLiab_GWP_and_assets(df, ratio_values)
    df = add_ratio_liab_others(df, ratio_values)
    df = make_employee_positive(df)
    df = group_by_region(df)
    df = add_number_null_values(df) #this makes prediction worse
    df = add_changes_yr1_yr0(df) #this makes predictions worse
    return df

def group_by_region(df):
    mapping = county_mapping
    df["Region"] = df["RegAddress.County"].map(mapping)
    #then delete original?
    #print(df["Region"].describe())
    #print(df["RegAddress.County"])
    #df = df.drop(labels=["RegAddress.County"], axis=1, inplace=False)
    return df
    #sys.exit(1)


def add_number_null_values(df):
    for year in YEARS:
        columns = get_col_names_containing([year], df)
        new_name = "Null_count." + year
        df[new_name] = 0
        for column in columns:
            df[new_name] = df[new_name] + df[column].isnull()
            #print(df[new_name].describe())
    return df

def add_changes_yr1_yr0(df):
    def add_change_yr1_yr0(df, column):
        """Calculates change year on year. If positive, it is an increase"""
        col_yr1 = column + ".yr1"
        col_yr0 = column + ".yr0"
        new_name = column + "_CHANGE"
        df = update_df_column_type(df, [col_yr1, col_yr0], "float64")
        temp1 = df[col_yr1].fillna(0, inplace=False)
        temp2 = df[col_yr0].fillna(0, inplace=False)
        df[new_name] = temp2 - temp1
        return df
    yr1 = get_col_names_containing(["yr1"], df)
    irrelevant = ['StartDateForPeriodCoveredByReport', 'EndDateForPeriodCoveredByReport', 'EntityDormant']
    for col in yr1:
        name = col.replace(".yr1", "")
        if name not in irrelevant:
            df = add_change_yr1_yr0(df, name)
    return df





def add_ratio_liab_others(df, ratio_values):
    df = combine_values(df, ratio_values, "TotalLiabilities", "TotalAssetsLessLiabilities_calculated", "Liabilities/Assets_calc")
    df = combine_values(df, ratio_values, "TotalLiabilities", "TotalAssetsLessCurrentLiabilities", "Liabilities/Assets")
    df = combine_values(df, ratio_values, "TotalLiabilities", "GWP", "Liabilities/GWP")
    df = combine_values(df, ratio_values, "TotalLiabilities", "ProvisionsForLiabilities", "Liabilities/Provisions")
    return df

def add_ratio_provLiab_GWP_and_assets(df, ratio_values):
    df = combine_values(df, ratio_values, "ProvisionsForLiabilities", "TotalAssetsLessLiabilities_calculated", "Provisions/Assets_calc")
    df = combine_values(df, ratio_values, "ProvisionsForLiabilities", "TotalAssetsLessCurrentLiabilities", "Provisions/Assets")
    df = combine_values(df, ratio_values, "ProvisionsForLiabilities", "GWP", "Provisions/GWP")
    return df

def add_ratio_employee_officers(df, ratio_values):
    df = combine_values(df, ratio_values, "AverageNumberEmployeesDuringPeriod", "NumberEntityOfficer", "Employees/Officer", inf_value=5)
    return df


def add_TotalLiabilities(df):
    def add_values(df, first, second, name):
        temp1 = df[first].fillna(0, inplace=False)
        temp2 = df[second].fillna(0, inplace=False)
        return temp1 + temp2
    df = combine_values(df, add_values, ":Creditors", "TotalBorrowings", "TotalLiabilities", inf_value=0)
    return df

def combine_values(df, fn, numerators_containing, denominators_containing, new_name, inf_value=100):
    """Combine values in same year"""
    numerators = get_col_names_containing([numerators_containing], df)
    denominators = get_col_names_containing([denominators_containing], df)
    df = update_df_column_type(df, numerators, "float64")
    df = update_df_column_type(df, denominators, "float64")
    for numerator in numerators:
        num_sufx = numerator.replace(numerators_containing, "")
        for denominator in denominators:
            denom_sufx = denominator.replace(denominators_containing, "")
            if num_sufx == denom_sufx:
                year = denom_sufx
                name =  new_name + year
                df[name] = fn(df, numerator, denominator, name)
                df[name].replace([np.inf, -np.inf], [inf_value, -inf_value], inplace=True)
                df[name].fillna(0, inplace=True)
    return df


def add_mortgage_ratio_outstanding(df):
    """To do with age of company"""
    df["Mortgages.RatioOutstanding"] = df["Mortgages.NumMortOutstanding"]/df["Mortgages.NumMortCharges"]
    #print(df[["Mortgages.RatioOutstanding", "Mortgages.NumMortOutstanding", "Mortgages.NumMortCharges"]])
    df["Mortgages.RatioOutstanding"].fillna(0, inplace=True)
    return df


def add_NumberSicCodes(df):
    sic_nums = [1,2,3,4]
    sic_cols = map(lambda x: "SICCode.SicText_{}".format(x), sic_nums)
    df["NumberSicCodes"] = 0
    for column in sic_cols:
        df["NumberSicCodes"] += df[column] != ""
    return df

def add_TotalAssetsLessLiabilities_calculated(df):
    def fn(df, year, assets_plus, assets_minus):
        df["TotalAssetsLessLiabilities_calculated.{}".format(year)] = 0
        for column in df.columns:
            if year in column:
                if column.replace(".{}".format(year), "") in assets_plus:
                    df = update_df_column_type(df, [column], "float64")
                    df["TotalAssetsLessLiabilities_calculated.{}"
                        .format(year)] = df["TotalAssetsLessLiabilities_calculated.{}".format(year)].add(df[column], fill_value=0)
                elif column.replace(".{}".format(year), "") in assets_minus:
                    df = update_df_column_type(df, [column], "float64")
                    df["TotalAssetsLessLiabilities_calculated.{}"
                        .format(year)] = df["TotalAssetsLessLiabilities_calculated.{}".format(year)].subtract(df[column], fill_value=0)
        return df

    assets_plus = ["ShareholderFunds", "FixedAssets", ":CurrentAssets", "CashBankOnHand", "ProvisionsForLiabilities",] #"FinancialAssets"
    assets_minus = [":Creditors", "TotalBorrowings"]

    df = fn(df, "yr1", assets_plus, assets_minus)
    df = fn(df, "yr0", assets_plus, assets_minus)
    return df

###################
#Remove irrelevant features
def remove_irrelevant_features(df):

    col_drop_policy = ["Insured Name", "Policy Number", "Division", "Company Number"]
    col_drop_accounts = ["filename", "directory_name", "UKCompaniesHouseRegisteredNumber", "EntityCurrentLegalOrRegisteredName"] #for all years
    poor_features = ["d:Equity.yr1"]
    drop_accounts_2 = []
    for value in col_drop_accounts:
        for key in df.keys():
            if value in key:
                drop_accounts_2.append(key)
    drop_labels = []
    drop_labels.extend(col_drop_policy)
    drop_labels.extend(drop_accounts_2)
    drop_labels.extend(poor_features)
    #drop_labels.extend(get_col_names_containing(["Sub Division", "Sub Portfolio", "Portfolio"], df))
    #drop_labels.extend(get_col_names_containing(["yr0"], df))
    return df.drop(labels=drop_labels, axis=1, inplace=False)

def save_temp_data(df, save_data, path=TEMP_SAVE1):
    if save_data:
        df.to_csv(path)
    return df

if __name__ == "__main__":
    run()
