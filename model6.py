
from model5 import run as model
from model5 import prepare_data
from model5 import parse_claim_GWP
import sys
import pandas as pd
import numpy as np
RUNS = 200


XLS_OUT = "results/profile_results1.xlsx"

def main():
    """
    RESULT: You can't. Kapp score is 0% - i.e. pure random.

    When a claim has occured. Look at if there is a way of predicting if ratio > 0.5.
    This may perform better. Only spend an hour.

    Then take the values where "claim" model predicts a claim and see if you can get
    better numbers on the output. """

    X, y, df_original = prepare_data(model="claim", numerical_bins=False)


    clf, X_train, X_test, y_train, y_test, y_pred = model(X, y, random_state=5)

    pred = clf.predict(X)

    X_P = X[pred]
    print(df_original.shape)
    df_P = df_original[pred]


    y_P = df_P["Claims"] / df_P["GWP"] > 0.5

    y_P = parse_claim_GWP(y_P) #remove NaN and inf
    print(y_P.describe())
    print()

    clf, X_train, X_test, y_train, y_test, y_pred = model(X_P, y_P, random_state=9)

    sys.exit()
    incorrect_X = X_test[y_pred != y_test]
    incorrect_y = y_test[y_pred != y_test]
    correct_X = X_test[y_pred == y_test]
    correct_y = y_test[y_pred == y_test]

    X_FP = incorrect_X[incorrect_y == False]
    X_TP = correct_X[correct_y == True]

    X_pred_P = pd.concat([X_FP, X_TP], axis=0)

    #y-values
    y_FP = incorrect_y[incorrect_y == True]
    y_TP = correct_y[correct_y == True]
    y_pred_P = pd.concat([y_FP, y_TP], axis=0)


    #print(X_pred_P)
    print(y_pred_P)
    sys.exit()

    xls_writer = pd.ExcelWriter(XLS_OUT)

    return


def find_repeating_values(dictionary, X, message, xls_writer):

    keys = []
    save_dict = {}
    for key, value in dictionary.items():
        if value > 0:
            keys.append(key)
            save_dict[key] = value

    values = X.iloc[keys]


    #values.loc[save_dict.keys(), "Count Occurences"] = values["Count Occurences"].map(save_dict)
    values["Count Occurences"] = values.index
    values["Count Occurences"] = values["Count Occurences"].map(save_dict)
    print(message, "=", len(values.index))
    print()
    values.to_excel(xls_writer, "{}".format(message))
    xls_writer.save()
    return values

def update_dictionary(df, dictionary):
    for key in df.index:
        try:
            dictionary[key] += 1
        except KeyError:
            dictionary[key] = 1
    return dictionary

if __name__ == "__main__":
    main()
