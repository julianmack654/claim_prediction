import pandas as pd
import requests
import sys
from io import BytesIO
import json
from DB_info import SQL_ARG
from sqlalchemy import create_engine, sql
from validation import validate_list_company_names, remove_company_stopwords
from PCMdb import PCMdb as db
import pandas as pd
import time
import datetime
from datetime import timedelta
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.tree import DecisionTreeClassifier


CSV_FILE = "data/policies_w_accounts.csv"
PCMdb = "QBE_policies_data"
PCM_comp_numbers = "QBE_company_numbers"
TABLE = PCMdb
QBE_2017_YEAR_END = datetime.datetime(2017, 12, 30)
DAYS_YEAR = 365
OUT_CSV = "data/policies_w_accounts_TEST.csv"

"""
80-20 split of data into train and test
X_train, y_train
X_test, y_test
"""

#ACCOUNT COLUMNS:
numeric_variables = [
"ProfitLoss",
"TotalAssetsLessCurrentLiabilities",
"ShareholderFunds",
"Turnover",
"FixedAssets",
":CurrentAssets",
":Creditors",
":NetCurrentAssets",
"CashBankOnHand",
"ProvisionsForLiabilities",
"d:Equity",
"TotalBorrowings",
"FinancialAssets",
"NumberEntityOfficer",
"AverageNumberEmployeesDuringPeriod"
]

non_zero_variables = [
"Turnover",
"ProfitLoss",
"NumberEntityOfficer",
"AverageNumberEmployeesDuringPeriod"
]

bool_variables = [
"EntityDormant"
]

date_variables = [
"StartDateForPeriodCoveredByReport",
"EndDateForPeriodCoveredByReport",
]



def main():
    engine = create_engine(SQL_ARG)
    with engine.connect() as conn:
        query = """SELECT *
                    FROM companies."QBE_policies_data_w_accounts_2yrs";"""
        df = pd.read_sql(query, conn)
        df = remove_irrelevant_features(df)
        #print(df.describe())
        #print(df.dtypes)
        print(df)
        df = preprocess_features(df)
        print(df)
        col_remove_y = ["Claims", "Claim Count"]
        X = df.drop(labels=col_remove_y, axis=1)
        y = df["Claims"] > 0
        print(X.describe())
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        from sklearn.ensemble import RandomForestClassifier
        n_estimators_options = [3**x for x in range(1,6)]
        criteria = ["entropy", "gini"]
        min_samples_splits = [2**x for x in range(1,4)]

        results = []
        for criterion in criteria:
            for n_estimators in n_estimators_options:
                for min_samples_split in min_samples_splits:
                    clf = RandomForestClassifier(n_estimators=n_estimators, min_samples_split=min_samples_split, criterion=criterion)
                    clf.fit(X_train, y_train)
                    train_score = clf.score(X_train, y_train)
                    test_score = clf.score(X_test, y_test)
                    result = {"n_estimators": n_estimators, "min_samples_split": min_samples_split,
                                "criterion": criterion, "train_score": train_score, "test_score": test_score}
                    results.append(result)
                    print(result)
        df = pd.DataFrame(results)
        print(df)
        xls_writer = pd.ExcelWriter("results/randomForest_2707.xlsx")
        df.to_excel(xls_writer, "Sheet 1")
        xls_writer.save()



        """
        from sklearn.metrics import classification_report
        from sklearn.metrics import confusion_matrix
        pred = clf.predict(X_test)
        print(confusion_matrix(y_test, pred))
        print(classification_report(y_test, pred))
        print(clf)
        """

        sys.exit(1)
        clf = DecisionTreeClassifier().fit(X_train, y_train)
        print('Accuracy of Decision Tree classifier on training set: {:.2f}'
             .format(clf.score(X_train, y_train)))
        print('Accuracy of Decision Tree classifier on test set: {:.2f}'
             .format(clf.score(X_test, y_test)))

        from sklearn.neighbors import KNeighborsClassifier
        knn = KNeighborsClassifier()
        knn.fit(X_train, y_train)
        print('Accuracy of K-NN classifier on training set: {:.2f}'
             .format(knn.score(X_train, y_train)))
        print('Accuracy of K-NN classifier on test set: {:.2f}'
             .format(knn.score(X_test, y_test)))

        from sklearn.linear_model import LogisticRegression
        logreg = LogisticRegression()
        logreg.fit(X_train, y_train)
        print('Accuracy of Logistic regression classifier on training set: {:.2f}'
             .format(logreg.score(X_train, y_train)))
        print('Accuracy of Logistic regression classifier on test set: {:.2f}'
             .format(logreg.score(X_test, y_test)))

        from sklearn.neighbors import KNeighborsClassifier
        knn = KNeighborsClassifier()
        knn.fit(X_train, y_train)
        print('Accuracy of K-NN classifier on training set: {:.2f}'
             .format(knn.score(X_train, y_train)))
        print('Accuracy of K-NN classifier on test set: {:.2f}'
             .format(knn.score(X_test, y_test)))

        from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
        lda = LinearDiscriminantAnalysis()
        lda.fit(X_train, y_train)
        print('Accuracy of LDA classifier on training set: {:.2f}'
             .format(lda.score(X_train, y_train)))
        print('Accuracy of LDA classifier on test set: {:.2f}'
             .format(lda.score(X_test, y_test)))

        from sklearn.naive_bayes import GaussianNB
        gnb = GaussianNB()
        gnb.fit(X_train, y_train)
        print('Accuracy of GNB classifier on training set: {:.2f}'
             .format(gnb.score(X_train, y_train)))
        print('Accuracy of GNB classifier on test set: {:.2f}'
             .format(gnb.score(X_test, y_test)))

        from sklearn.svm import SVC
        svm = SVC()
        svm.fit(X_train, y_train)
        print('Accuracy of SVM classifier on training set: {:.2f}'
             .format(svm.score(X_train, y_train)))
        print('Accuracy of SVM classifier on test set: {:.2f}'
             .format(svm.score(X_test, y_test)))


def remove_irrelevant_features(df):
    col_drop_policy = ["Insured Name", "Policy Number", "Division"]
    col_drop_accounts = ["filename", "directory_name", "UKCompaniesHouseRegisteredNumber", "EntityCurrentLegalOrRegisteredName"] #for all years
    col_drop_accounts.extend(date_variables)
    drop_accounts_2 = []
    for value in col_drop_accounts:
        for key in df.keys():
            if value in key:
                drop_accounts_2.append(key)
    drop = []
    drop.extend(col_drop_policy)
    drop.extend(drop_accounts_2)
    return df.drop(labels=drop, axis=1, inplace=False)

def preprocess_features(df):
    df = update_df_column_type(df, numeric_variables, "float64")

    for variable in bool_variables:
        for column in df.keys():
            if variable in column:
                df[column].loc[pd.isnull(df[column])] = False

    df = numerical_variables_update(df)

    #replace missing values
    #put into bins
    #categoric features

    le=LabelEncoder()
    ohe=OneHotEncoder(sparse=False)
    for col in df.columns.values:
        # Encoding only categorical variables
        if df[col].dtypes=='object':
            le.fit(df[col])
            new_features = le.transform(df[col])
            new_features = new_features.reshape(-1,1)
            ohe.fit(new_features)
            temp = ohe.transform(new_features)
            temp=pd.DataFrame(temp, columns=[(col+"_"+str(i)) for i in df[col].value_counts().index])
            temp=temp.set_index(df.index.values)
            df = pd.concat([df,temp], axis=1)
            df = df.drop(labels=col, axis=1)
    return df

def numerical_variables_update(df):
    variables = [x for x in numeric_variables if x not in non_zero_variables]
    variables = numeric_variables
    for variable in variables:
        for column in df.keys():
            if variable in column:
                df[column] = df[column].fillna(value=0)
    return df


def update_df_column_type(df, columns, new_type):
    for variable in columns:
        for column in df.keys():
            if variable in column:
                df[column] = df[column].astype(new_type)
    return df

if __name__ == "__main__":
    main()
