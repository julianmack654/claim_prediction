import pandas as pd
import requests
import sys
from io import BytesIO
import json
from DB_info import SQL_ARG
from sqlalchemy import create_engine, sql
from validation import validate_list_company_names, remove_company_stopwords
from PCMdb import PCMdb as db
import pandas as pd
import time
import datetime
from datetime import timedelta
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

CSV_FILE = "data/policies_w_accounts.csv"
PCMdb = "QBE_policies_data"
PCM_comp_numbers = "QBE_company_numbers"
TABLE = PCMdb
QBE_2017_YEAR_END = datetime.datetime(2017, 12, 30)
DAYS_YEAR = 365

OUT_CSV = "data/policies_w_accounts_TEST.csv"

"""
80-20 split of data into train and test
X_train, y_train
X_test, y_test
"""
def main():
    df = pd.read_csv(OUT_CSV)
    #df = df.drop(columns=0)
    col_drop_policy = ["Claims", "Claim Count", "Insured Name", "Policy Number"]
    col_drop_companies_db = [] #do this in original call to db
    X = df.drop(labels=col_drop, axis=1)
    #print(X)
    for value in X.keys():
        print(value)

    sys.exit(2)
    y = df["Claims"] > 0
    print(y)
    sys.exit(2)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    clf = DecisionTreeClassifier().fit(X_train, y_train)
    print('Accuracy of Decision Tree classifier on training set: {:.2f}'
         .format(clf.score(X_train, y_train)))
    print('Accuracy of Decision Tree classifier on test set: {:.2f}'
         .format(clf.score(X_test, y_test)))






if __name__ == "__main__":
    main()
