import pandas as pd
import sys
from io import BytesIO
import json
from DB_info import SQL_ARG
from sqlalchemy import create_engine, sql
from validation import validate_list_company_names, remove_company_stopwords
from PCMdb import PCMdb as db
import pandas as pd
import time
import random


PCMdb = "QBE_policies_data"
PCM_comp_numbers = "QBE_company_numbers"
TABLE = PCMdb

DOORDA_API_KEY = "L1ORdektGNzrBZZpgW7dEwpthgxkRl46"
DOORDA_HEADERS = {"apikey": DOORDA_API_KEY}
COMPANY_NUMBER_URL = "https://api.doorda.com/v1/company/company_name/"
input_xlsx_path = "PCMxls/PCMinformationNanRemoved.xlsx"
output_xlsx_path = "PCMxls/PCMinformationNanRemoved.xlsx"

class ComanyNumberFind():
    def __init__(self):
        self.companies_dict = self.get_comp_numbers_dict_from_db()
        companies_list = self.companies_dict.keys()
        self.companies_list = [x for x in companies_list if "$" not in x]

    def find(self, company_name):
        name = remove_company_stopwords(company_name)
        try: #try to see if it is exactly right (worth a try!)
            number = self.companies_dict[name]
            return number
        except KeyError:
            registered_name = validate_list_company_names(name, self.companies_list)
            if registered_name:
                return self.companies_dict[registered_name]

    def get_comp_numbers_dict_from_db(self):
        engine = create_engine(SQL_ARG)
        company_numbers_dict = {}
        with engine.connect() as conn:
            query = """SELECT "CompanyName", "CompanyNumber"
                        FROM companies.companies_house
                        LIMIT 100;
                        """
            query = """SELECT "CompanyName", "CompanyNumber"
                        FROM companies.companies_house;
                        """
            result = conn.execute(query).fetchall()
            for company in result:
                CompanyName, CompanyNumber = company
                CompanyName = remove_company_stopwords(CompanyName)
                CompanyNumber = str(CompanyNumber)
                company_numbers_dict[CompanyName] = CompanyNumber

        return company_numbers_dict


def main():
    engine = create_engine(SQL_ARG)
    with engine.connect() as conn:
        unique_names = get_unique_names(conn)
        number_unique = len(unique_names)
        print(number_unique, "company numbers left to find")
        company_numbers_db = ComanyNumberFind()
        print("Full companies db loaded")
        number_found = 0
        while number_unique > 0:
            unique_names = get_unique_names(conn)
            number_unique = len(unique_names)
            name = unique_names[random.randint(0, number_unique)]
            company_number = company_numbers_db.find(name)
            if not company_number:
                print("Company number not found for:", name)
                company_number = "UNKNOWN"
            else:
                print("company number:", company_number, "found for", name)
                number_found +=1
            if company_number:
                name = parse_for_SQL(name)
                query = """UPDATE companies."QBE_policies_data"
                            SET "Company Number" = '{}'
                            WHERE "Insured Name" = '{}';""".format(company_number, name)
                conn.execute(query)

        print(number_found, "new company numbers found and added")





def parse_for_SQL(value):
    value = value.replace("'", "''")
    return value
def doorda_comany_numbers_call(name):
    url = COMPANY_NUMBER_URL + name
    r = requests.get(url=url, headers=DOORDA_HEADERS)
    r.encoding = "utf-8"
    if r.status_code == 200:
        json = r.json()
        try:
            company_number = json["results"][0]["company_number"]
            return company_number
        except:
            return None
    else:
        #print("timed out")
        return None

def get_unique_names(conn):
    query = """ SELECT "Insured Name"
                FROM companies."QBE_policies_data"
                WHERE "Company Number" IS NULL;
                """
    results = conn.execute(query).fetchall()
    names = []
    for row in results:
        name = row[0]
        names.append(name)
    unique_names = list(set(names))
    return unique_names

if __name__ == "__main__":
    main()
