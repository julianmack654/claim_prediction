from model5 import run as model
from model5 import prepare_data
import sys
import pandas as pd
import numpy as np
RUNS = 150


XLS_OUT_1 = "results/profile_results_ratioNobins.xlsx"
XLS_OUT_2 = "results/profile_results_claimwbins.xlsx"



def main():
    xls_writer = pd.ExcelWriter(XLS_OUT_1)
    X, y, df_original = prepare_data(model="ratio", numerical_bins=False)

    t_p = {}
    t_n = {}
    f_p = {}
    f_n = {}


    for i in range(RUNS):
        clf, X_train, X_test, y_train, y_test, y_pred = model(X, y, random_state=5*i)

        incorrect_X = X_test[y_pred != y_test]
        incorrect_y = y_test[y_pred != y_test]
        correct_X = X_test[y_pred == y_test]
        correct_y = y_test[y_pred == y_test]

        false_positives = incorrect_X[incorrect_y == False]
        false_negatives = incorrect_X[incorrect_y == True] #most important
        true_positives = correct_X[correct_y == True] #also important
        true_negatives = correct_X[correct_y == False]

        f_p = update_dictionary(false_positives, f_p)
        f_n = update_dictionary(false_negatives, f_n)
        t_p = update_dictionary(true_positives, t_p)
        t_n = update_dictionary(true_negatives, t_n)

        #find_repeating_values(t_p, X, "True Positives", xls_writer)



    f_p = find_repeating_values(f_p, df_original, "False Positives", xls_writer)
    f_n = find_repeating_values(f_n, df_original, "False Negatives", xls_writer)
    t_p = find_repeating_values(t_p, df_original, "True Positives", xls_writer)
    t_n = find_repeating_values(t_n, df_original, "True Negatives", xls_writer)


    xls_writer = pd.ExcelWriter(XLS_OUT_2)
    X, y, df_original = prepare_data(model="claim", numerical_bins=True)


    t_p = {}
    t_n = {}
    f_p = {}
    f_n = {}


    for i in range(RUNS):
        clf, X_train, X_test, y_train, y_test, y_pred = model(X, y, random_state=5*i)

        incorrect_X = X_test[y_pred != y_test]
        incorrect_y = y_test[y_pred != y_test]
        correct_X = X_test[y_pred == y_test]
        correct_y = y_test[y_pred == y_test]

        false_positives = incorrect_X[incorrect_y == False]
        false_negatives = incorrect_X[incorrect_y == True] #most important
        true_positives = correct_X[correct_y == True] #also important
        true_negatives = correct_X[correct_y == False]

        f_p = update_dictionary(false_positives, f_p)
        f_n = update_dictionary(false_negatives, f_n)
        t_p = update_dictionary(true_positives, t_p)
        t_n = update_dictionary(true_negatives, t_n)

        #find_repeating_values(t_p, X, "True Positives", xls_writer)



    f_p = find_repeating_values(f_p, df_original, "False Positives", xls_writer)
    f_n = find_repeating_values(f_n, df_original, "False Negatives", xls_writer)
    t_p = find_repeating_values(t_p, df_original, "True Positives", xls_writer)
    t_n = find_repeating_values(t_n, df_original, "True Negatives", xls_writer)
    return


def find_repeating_values(dictionary, X, message, xls_writer):

    keys = []
    save_dict = {}
    for key, value in dictionary.items():
        if value > 0:
            keys.append(key)
            save_dict[key] = value

    values = X.iloc[keys]

    #values.loc[save_dict.keys(), "Count Occurences"] = values["Count Occurences"].map(save_dict)
    values["Count Occurences"] = values.index
    values["Count Occurences"] = values["Count Occurences"].map(save_dict)
    print(message, "=", len(values.index))
    print()
    extra = values.describe()
    values.to_excel(xls_writer, "{}".format(message))
    extra.to_excel(xls_writer, "{}_Analysis".format(message))
    xls_writer.save()
    return values

def update_dictionary(df, dictionary):
    for key in df.index:
        try:
            dictionary[key] += 1
        except KeyError:
            dictionary[key] = 1
    return dictionary

if __name__ == "__main__":
    main()
