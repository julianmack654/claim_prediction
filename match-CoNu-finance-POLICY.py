import pandas as pd
import sys
from io import BytesIO
import json
from DB_info import SQL_ARG
from sqlalchemy import create_engine, sql
from PCMdb import PCMdb as db
import pandas as pd
import time
import datetime
from datetime import timedelta

TABLE_JOINED = "QBE_policies_data_w_accounts_2yrs"
OUT_CSV = "data/policies_w_accounts_TEST2.csv"
PCMdb = "QBE_policies_data"
PCM_comp_numbers = "QBE_company_numbers"
TABLE = PCMdb
QBE_2017_YEAR_END = datetime.datetime(2017, 12, 30)
DAYS_YEAR = 365
POSSIBLE_ACCOUNT_YEARS = {
    3: "yr3",    #"2014"
    2: "yr2",    #"2015"
    1: "yr1",    #"2016" - year before underwriting - MOST RELEVANT
    0: "yr0",    #"2017" - underwriting year
    -1: "yr-1",   #"2018" - not useful for leading indicators
    -2: "yr-2"    #"2019" - not useful for leading indicators
}


def main():
    #match policies with financial data
    #CHECK:
        #Are there accounts for whole period (if not newly incorporated)?
        #fill in missing values if possible
        #Are the accounts a full set?
    #create df of complete entries
    #and print
    #could run model at this point
    engine = create_engine(SQL_ARG)
    with engine.connect() as conn:
        query = """SELECT "Policy Number", "Insured Name", "Industry Section", "Banding", "Division", "Sub Division",
                        "Portfolio", "Sub Portfolio", "GWP", "Policy Count", "Claims", "Claim Count", "Company Number",
                         "RegAddress.County", "CompanyCategory", "CompanyStatus", "CountryOfOrigin",
                        "DissolutionDate", "IncorporationDate", "Accounts.AccountCategory", "Mortgages.NumMortCharges",
                        "Mortgages.NumMortOutstanding", "Mortgages.NumMortPartSatisfied", "Mortgages.NumMortSatisfied",
                        "SICCode.SicText_1", "SICCode.SicText_2", "SICCode.SicText_3", "SICCode.SicText_4"
                    FROM companies."QBE_policies_data"
                    inner join companies."companies_house" on
                    companies."QBE_policies_data"."Company Number" = companies."companies_house"."CompanyNumber"
                    where companies."QBE_policies_data"."Company Number" != 'UNKNOWN'"""
        results = conn.execute(query).fetchall()
        print(len(results))
        columns = conn.execute(query).keys()
        df_policies = pd.DataFrame(results, columns=columns)
        num_rows, _ = df_policies.shape
        df_final = pd.DataFrame() #empty df
        print(df_policies)
        count = 0
        count_sucessful = 0
        for index_policy, policy in df_policies.iterrows():
            count +=1
            company_number = policy["Company Number"].strip()
            if company_number:
                df_accounts = get_company_accounts(company_number, conn)
                df_accounts = df_accounts.drop_duplicates()
                dict_accounts = split_accounts_by_year(df_accounts)
                yr_previous, yr_underwrite = get_accounts_by_year(dict_accounts)
                if not yr_previous.empty and not yr_underwrite.empty:
                    full_row_elements = [policy, yr_previous, yr_underwrite]
                    full_row = policy.append(yr_previous).append(yr_underwrite)
                    df_final = pd.concat([df_final, full_row], axis=1, sort=False)
                    count_sucessful += 1
                    percentage = count_sucessful/count*100
                    print("Found {}/{} from total of {}. Percentage success: {:.2f}%".format(count_sucessful, count, num_rows, percentage))
                else: #incomplete data - omit policy
                    percentage = count_sucessful/count*100
                    print("Found {}/{} from total of {}. Percentage success: {:.2f}%".format(count_sucessful, count, num_rows, percentage))

        df_final = df_final.transpose()
        print("Final df:")
        print(df_final)
        df_final.to_sql(TABLE_JOINED, con=engine, if_exists='append',schema='companies',index=False)
        df_final.to_csv(OUT_CSV)
        print("Total number of policies in training set:")
        print(df_final.shape)




def split_accounts_by_year(df_accounts):
    df_accounts = df_accounts.sort_values('EndDateForPeriodCoveredByReport', ascending=False)
    dict_accounts = {}
    for index, row in df_accounts.iterrows():
        row = fill_missing_values(row)
        if not row.empty:
            days_interval = find_year(row['EndDateForPeriodCoveredByReport'])
            try:
                year = int(days_interval / DAYS_YEAR) #round to nearest year
                dict_accounts[year] = row
            except (ValueError, TypeError): #for NaN 'EndDateForPeriodCoveredByReport'
                pass #throw away this accounts data - use filename to get date
    return dict_accounts

def fill_missing_values(row):
    #TODO
    #check if missing
    #fill in those that can be filled
    #if not possible, return empty series
    return row

def find_year(date):
    if date:
        interval = QBE_2017_YEAR_END - date
        return interval.days
    else:
        return None

def check_year_accounts(df_accounts):
    end_dates = df_accounts["EndDateForPeriodCoveredByReport"].values
    results = []
    for name in file_names:
        pass

def generate_test_df():
    engine = create_engine(SQL_ARG)
    with engine.connect() as conn:
        query = """select *
                    FROM companies.test_companies_house;"""
        results = conn.execute(query).fetchall()
        columns = conn.execute(query).keys()
        df = pd.DataFrame(results, columns=columns)
    return df


def get_company_accounts(company_number, conn):
    company_number = parse_for_SQL(company_number)
    query = """SELECT *
                FROM companies."companies_house_pdf_parsed_ALL"
                where "UKCompaniesHouseRegisteredNumber" = '{}';""".format(company_number)
    results = conn.execute(query).fetchall()
    columns = conn.execute(query).keys()
    return pd.DataFrame(results, columns=columns)

def parse_for_SQL(value):
    value = value.replace("'", "''")
    return value


def get_accounts_by_year(dict_accounts):
    try:
        yr_previous = dict_accounts[1]
    except KeyError:
        try:
            yr_previous = dict_accounts[2]
        except KeyError:
            try:
                yr_previous = dict_accounts[3]
            except KeyError:
                yr_previous = pd.Series()
    try:
        yr_underwrite = dict_accounts[0]
    except KeyError:
        try:
            yr_underwrite = dict_accounts[-1]
        except KeyError:
            try:
                yr_underwrite = dict_accounts[-2]
            except KeyError:
                yr_underwrite = pd.Series()
    yr_previous = change_col_headings(yr_previous, ".yr1")
    yr_underwrite = change_col_headings(yr_underwrite, ".yr0")
    return yr_previous, yr_underwrite
def change_col_headings(series, new_str):
    new_headings = {}
    for index in series.keys():
        new_index = "{}{}".format(index, new_str)
        new_headings[index] = new_index
    series = series.rename(new_headings)
    return series

if __name__ == "__main__":
    main()
