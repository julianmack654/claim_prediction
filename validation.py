"""All functions take either:
a) x, y as two instances to compare
b) x, ys as list of x values to compare to y, with an optional limit on number of values to return
and optional threshold (out of 100)"""

from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import string
import sys
import string


COMPANY_NAME_THRESHOLD = 93
COMPANY_STOPWORDS = [
    "ltd", "limited",
    "cyfyngedig", "cyf",
    "public limited company", "plc",
    "cwmni cyfyngedig cyhoeddus","ccc",
    "limited liability partnership", "llp",
    "community interest company", "cic",
    "partner", "partnership"
]


def validate_company_name(x, y, threshold=COMPANY_NAME_THRESHOLD):
    x, y = map(remove_company_stopwords, [x, y])
    return fuzz.ratio(x, y) >= threshold

def validate_list_company_names(x, ys, threshold=COMPANY_NAME_THRESHOLD):
    name, score = process.extractOne(x, ys, scorer=fuzz.ratio)
    if score > threshold:
        print(name, "is match for ", x, "with score of", score)
        return name

def remove_company_stopwords(company_name):
    """Removes regularly occuring company words"""
    name = remove_punct(company_name.lower()).strip()
    for stopword in COMPANY_STOPWORDS:
        name = name.replace(stopword, "")
    return name.upper()

def remove_punct(text):
    """Removes punctuation"""
    return text.translate(text.maketrans("", "", string.punctuation))
