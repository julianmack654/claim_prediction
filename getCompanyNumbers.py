import pandas as pd
import requests
import sys
from io import BytesIO
import json
from DB_info import SQL_ARG
from sqlalchemy import create_engine, sql
from validation.validation import validate_list_company_names, remove_company_stopwords
from PCMdb import PCMdb as db
import pandas as pd

PCMdb = "QBE_policies_data"
PCM_comp_numbers = "QBE_company_numbers"
TABLE = PCMdb
 
DOORDA_API_KEY = "L1ORdektGNzrBZZpgW7dEwpthgxkRl46"
DOORDA_HEADERS = {"apikey": DOORDA_API_KEY}
COMPANY_NUMBER_URL = "https://api.doorda.com/v1/company/company_name/"
input_xlsx_path = "PCMxls/PCMinformationNanRemoved.xlsx"
output_xlsx_path = "PCMxls/PCMinformationNanRemoved.xlsx"

def main():
    sys.exit(1) #to prevent db being updated
    df = pd.read_excel(input_xlsx_path, sheet_name="InformationParsed")
    df["Insured Name"] = df["Insured Name"].str.upper()
    #df = df.head(10)
    names = df["Insured Name"].values
    stripped_names = []
    unique_names = list(set(names))
    print("policies: ", len(names))
    print("companies: ", len(unique_names))
    registered_company_no_dict = get_comp_numbers_dict_from_db()
    print("Got company numbers dictionary from db")
    """count = 0
    for key, value in registered_company_no_dict.items():
        print(key, value)
        count += 1
        if count > 100:
            break"""
    PCM_company_number_dict = {}
    count_failure = 0
    count_total = 0
    for name in unique_names:
        count_total += 1
        #if count_total % 10 == 0:
            #print("{} searched so far".format(count_total))
        company_number = return_comp_numbers(name, registered_company_no_dict)
        if company_number:
            PCM_company_number_dict[name] = company_number
            continue
        else: #make doorda call
            count_failure += 1
            if False:
                PCM_company_number_dict[name] = None
                doorda_comany_numbers_call(name)
    print ("Number Failures:", count_failure, "/", len(unique_names))
    #create df and do properly
    series = pd.Series(df["Insured Name"].values, name="Company Number")
    #PCM_company_number_dict = {"THE GOODWOOD ESTATE CO LTD": 234, "THE ALL ENGLAND LAWN TENNIS & CROQUET CLUB LTD": 567, "LAMPTON FACILITIES MANAGEMENT 360 LTD": 4}
    def find_number(name):
        try:
            return PCM_company_number_dict[name]
        except KeyError:
            return None

    df2 = pd.DataFrame(series.map(find_number))
    df_final = df.merge(df2, how="left", left_index=True, right_index=True)
    #print(df_final)
    print("created final df")
    engine = create_engine(SQL_ARG)
    df_final.to_sql(TABLE, con=engine, if_exists='append',schema='companies',index=False)


def get_comp_numbers_dict_from_db():
    engine = create_engine(SQL_ARG)
    company_numbers_dict = {}
    with engine.connect() as conn:
        query = """SELECT "CompanyName", "CompanyNumber"
                    FROM companies.companies_house
                    LIMIT 10;
                    """
        query = """SELECT "CompanyName", "CompanyNumber"
                    FROM companies.companies_house;
                    """
        result = conn.execute(query).fetchall()
        for company in result:
            CompanyName, CompanyNumber = company
            CompanyName = remove_company_stopwords(CompanyName)
            #print(CompanyName)
            CompanyNumber = str(CompanyNumber)
            company_numbers_dict[CompanyName] = CompanyNumber
    #print(company_numbers_dict)
    return company_numbers_dict


def return_comp_numbers(name, company_numbers_dict):
    name = remove_company_stopwords(name)
    #print(name)
    try: #try to see if it is exactly right (worth a try!)
        number = company_numbers_dict[name]
        return number
    except KeyError:
        return None


        registered_name = validate_list_company_names(name, company_numbers_dict.keys())
        if registered_name:
            return company_numbers_dict[registered_name]

def doorda_comany_numbers_call(name):
    return
    #if not company_number:
    url = COMPANY_NUMBER_URL + name
    r = requests.get(url=url, headers=DOORDA_HEADERS)
    r.encoding = "utf-8"
    if r.status_code == 200:
        json = r.json()
        try:
            company_number = json["results"][0]["company_number"]
            print(company_number)
        except:
            pass
    else:
        print("timed out")


if __name__ == "__main__":
    main()
