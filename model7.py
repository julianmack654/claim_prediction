from model5 import run as model
from model5 import prepare_data
from model5 import parse_claim_GWP
import sys
import pandas as pd
import numpy as np



XLS_OUT = "results/profile_results1.xlsx"

def main():
    """
    """

    X, y, df_original = prepare_data(model="claim", numerical_bins=True, save_data=True)
    print()
    print("Prediction of Claim occuring: ")
    clf, X_train, X_test, y_train, y_test, y_pred = model(X, y, random_state=19)

    sys.exit(2)

    X, y, df_original = prepare_data(model="ratio", numerical_bins=False, save_data=True)
    print()
    print("Prediction that (claim size)/(GWP spent on policy) > 0.5 : ")
    clf, X_train, X_test, y_train, y_test, y_pred = model(X, y, random_state=5, save_features=True)
    print()
    print()


    sys.exit(2)


    sys.exit()
    from sklearn.svm import SVC
    from sklearn.metrics import cohen_kappa_score
    from sklearn.metrics import classification_report
    from sklearn.metrics import confusion_matrix
    from imblearn.over_sampling import RandomOverSampler
    X_resampled, y_resampled = RandomOverSampler(random_state=9).fit_sample(X_train, y_train)    #random oversampling
    #X_resampled, y_resampled = SMOTE().fit_sample(X_train, y_train) #SMOTE oversampling
    #X_resampled, y_resampled = ADASYN().fit_sample(X_train, y_train) #SMOTE oversampling
    X_train = pd.DataFrame(X_resampled)
    y_train = pd.Series(y_resampled)
    X_train.columns = X.columns
    svm = SVC(class_weight="balanced")
    svm.fit(X_train, y_train)
    pred = svm.predict(X_test)
    k_score = cohen_kappa_score(pred, y_test)
    print('Accuracy of SVM classifier on training set: {:.2f}'
         .format(svm.score(X_train, y_train)))
    print('Accuracy of SVM classifier on test set: {:.2f}'
         .format(svm.score(X_test, y_test)))
    print("Kappa score: {:.2f}%".format( k_score))
    print(confusion_matrix(y_test, pred))
    print(classification_report(y_test, pred))




if __name__ == "__main__":
    main()
