import pandas as pd
import requests
import sys
from io import BytesIO
import json
from DB_info import SQL_ARG
from sqlalchemy import create_engine, sql
from validation import validate_list_company_names, remove_company_stopwords
from features import numeric_continuous_variables, non_zero_int_variables, bool_variables, date_variables
from PCMdb import PCMdb as db
import pandas as pd
import numpy as np
import time
import datetime
from datetime import timedelta
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from imblearn.over_sampling import RandomOverSampler
from imblearn.over_sampling import SMOTE
from imblearn.over_sampling import ADASYN
from treeinterpreter import treeinterpreter as ti
from sklearn.metrics import cohen_kappa_score


PCMdb = "QBE_policies_data"
PCM_comp_numbers = "QBE_company_numbers"
TABLE = PCMdb
QBE_2017_YEAR_END = datetime.datetime(2017, 12, 30)
DAYS_YEAR = 365


"""
80-20 split of data into train and test
X_train, y_train
X_test, y_test
"""
#Processing data
NUMBER_BINS_NUMERICAL = 10#+1 for NaN
BIN_INTERVALS_EMPLOYEEE = pd.IntervalIndex([
    pd.Interval(0, 0),
    pd.Interval(0, 1),
    pd.Interval(1, 5),
    pd.Interval(5, 10),
    pd.Interval(15, 50),
    pd.Interval(50, 250),
    pd.Interval(250, 1000),
    pd.Interval(1000, 2500),
    ])
BIN_INTERVALS_ENTITY_OFFICERS = pd.IntervalIndex([
    pd.Interval(0, 0),
    pd.Interval(0, 1),
    pd.Interval(1, 3),
    pd.Interval(3, 7),
    pd.Interval(7, 15),
    pd.Interval(15, 90),
    ])
BIN_INTERVALS_MORTGAGES = pd.IntervalIndex([
    pd.Interval(0, 0),
    pd.Interval(0, 1),
    pd.Interval(1, 3),
    pd.Interval(3, 7),
    pd.Interval(7, 30),
    pd.Interval(30, 150),
    ])


#Random Forest model parameters
n_estimators=300
min_samples_split=3
criterion="gini"
#criterion="mse"

n_estimators_options = [3**x for x in range(5)]
criteria = ["entropy", "gini"]
min_samples_splits = [2**2 for x in range(4)]


def run():
    engine = create_engine(SQL_ARG)
    with engine.connect() as conn:
        query = """SELECT *
                    FROM companies."QBE_policies_data_w_accounts_2yrs";"""
        df = pd.read_sql(query, conn)

    df = remove_irrelevant_features(df)
    df = add_domain_specific_features(df)
    df = preprocess_features(df)
    #df = scale(df) #- feature scaling. But am putting numerical values in bins
    #print(df)
    col_remove_y = ["Claims", "Claim Count", "Banding", "GWP"]
    X = df.drop(labels=col_remove_y, axis=1)
    y = df["Claims"] / df["GWP"] > 0.5
    y = parse_claim_GWP(y) #remove NaN and inf

    #y = df["Claims"] > 0

    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    X_resampled, y_resampled = RandomOverSampler(random_state=9).fit_sample(X_train, y_train)    #random oversampling
    #X_resampled, y_resampled = SMOTE().fit_sample(X_train, y_train) #SMOTE oversampling
    #X_resampled, y_resampled = ADASYN().fit_sample(X_train, y_train) #SMOTE oversampling
    X_train = pd.DataFrame(X_resampled)
    y_train = pd.Series(y_resampled)
    X_train.columns = X.columns

    weights = create_weightings(y_train.copy())
    #weights = weights.iloc[::-1]
    #weights = None
    clf, features_df = run_random_forest(X_train, X_test, y_train, y_test, weights=weights, return_features=True)
    features_df.to_csv("results/randomForest_important_features_.csv")

    y_pred = clf.predict(X_test)
    incorrect_X = X_test[y_pred != y_test]
    incorrect_y = y_test[y_pred != y_test]


    relevant_features = features_df.head(200).keys()
    name = "Just with relevant features"
    clf = run_random_forest(X_train[relevant_features], X_test[relevant_features], y_train, y_test, weights=weights, message=name)

    pred = clf.predict(X[relevant_features])
    k_score = cohen_kappa_score(pred, y)

    print("Original data (full set):")
    print("Kappa score: {:.2f}%".format(k_score))
    print(confusion_matrix(y, pred))
    print(classification_report(y, pred))


def run_random_forest(X_train, X_test, y_train, y_test, weights=None, return_features = False, message = None):
    clf = RandomForestClassifier(n_estimators=n_estimators, min_samples_split=min_samples_split, criterion=criterion, n_jobs=-1)

    clf.fit(X_train, y_train, sample_weight=weights)
    train_score = clf.score(X_train, y_train)
    test_score = clf.score(X_test, y_test)
    pred = clf.predict(X_test)
    k_score = cohen_kappa_score(pred, y_test)
    print()
    print()
    if message:
        print(message)
    print('Accuracy on training set: {:.2f}%'
         .format(clf.score(X_train, y_train)))
    print('Accuracy on test set: {:.2f}%'
         .format(clf.score(X_test, y_test)))
    print("Kappa score: {:.2f}%".format( k_score))
    print(confusion_matrix(y_test, pred))
    print(classification_report(y_test, pred))
    if return_features:
        features_df = pd.Series(clf.feature_importances_, index=X_train.keys())
        features_df = features_df.sort_values(ascending=False)
        return clf, features_df
    print()
    print()
    return clf

def create_weightings(y):
    counts = y.value_counts()
    weights = {}
    base = 10
    for key in counts.keys():
        if key == True:
            multiplier = base*100
        else:
            multiplier = base
        weights[key] = 1/counts[key]*multiplier
    y.replace(weights, inplace=True)
    #print(pd.DataFrame([z,y]).transpose())
    return y

def parse_claim_GWP(y):
    y.rename("Claim/Cost ratio", inplace=True)
    y.fillna(0, inplace=True)
    REPLACE_INF = 1
    y.replace(np.inf, REPLACE_INF, inplace=True)
    return y



def preprocess_features(df):
    for variable in bool_variables:
        for column in df.keys():
            if variable in column:
                df[column].loc[pd.isnull(df[column])] = False
    #df = preprocess_numeric_features_replace_missing(df)
    df = preprocess_date_features(df)
    df = preprocess_numeric_features(df)
    le=LabelEncoder()
    ohe=OneHotEncoder(sparse=False)
    for col in df.columns.values:
        # Encoding only categorical variables
        if df[col].dtypes=='object':
            le.fit(df[col])
            new_features = le.transform(df[col])
            new_features = new_features.reshape(-1,1)
            ohe.fit(new_features)
            temp = ohe.transform(new_features)
            temp=pd.DataFrame(temp, columns=[(col+"_"+str(i)) for i in df[col].value_counts().index])
            temp=temp.set_index(df.index.values)
            df = pd.concat([df,temp], axis=1)
            df = df.drop(labels=col, axis=1)
    return df
def add_domain_specific_features(df):
    df = add_TotalAssetsLessLiabilities_calculated(df)
    df = add_NumberSicCodes(df)
    return df


def preprocess_date_features(df):
    def fn(x):
        if x == np.nan or x == None:
            return x
        else:
            try:
                final = (QBE_2017_YEAR_END - x).days
            except TypeError:
                y = datetime.datetime(x.year, x.month, x.day)
                final = (QBE_2017_YEAR_END - y).days
            return final
    #df["DissolutionDate"] = datetime.datetime(df["DissolutionDate"])
    date_columns = get_col_names_containing(date_variables, df)
    for column in date_columns:
        df[column] = df[column].apply(fn)
    return df


def preprocess_numeric_features(df):

    df = make_employee_positive(df)
    num_var_col = get_col_names_containing(numeric_continuous_variables, df)
    date_var_col = get_col_names_containing(date_variables, df)
    int_var_col = get_col_names_containing(non_zero_int_variables, df)
    num_var_col.extend(date_var_col)
    num_var_col.extend(int_var_col)
    df = update_df_column_type(df, num_var_col, "float64")
    for column in num_var_col:
        if "Mortgages" in column:
            df[column] = pd.cut(df[column], bins=BIN_INTERVALS_MORTGAGES)
        elif "AverageNumberEmployeesDuringPeriod" in column:
            df[column] = pd.cut(df[column], bins=BIN_INTERVALS_EMPLOYEEE)
        elif "NumberEntityOfficer" in column:
            df[column] = pd.cut(df[column], bins=BIN_INTERVALS_ENTITY_OFFICERS)
        else:
            df[column] = pd.qcut(df[column], NUMBER_BINS_NUMERICAL, duplicates="drop")

    df = update_df_column_type(df, num_var_col, "object")
    df[num_var_col] = df[num_var_col].fillna(pd.Interval(-0.1, 0.1)) #for NaN

    return df


def update_df_column_type(df, columns, new_type):
    for variable in columns:
        for column in df.keys():
            if variable in column:
                df[column] = df[column].astype(new_type)
    return df

def get_col_names_containing(variables_list, df):
    columns_final = []
    for variable in variables_list:
        min_samples_splits = [2**x for x in range(1,4)]
        columns_final.extend([x for x in df.keys() if variable in x])
    return columns_final

def make_employee_positive(df):
    def fn(x):
        if x == np.nan or x == None:
            return x
        else:
            return abs(float(x))
    columns = get_col_names_containing(non_zero_int_variables, df)
    for column in columns:
        df[column] = df[column].apply(fn)
    return df

def add_NumberSicCodes(df):
    sic_nums = [1,2,3,4]
    sic_cols = map(lambda x: "SICCode.SicText_{}".format(x), sic_nums)
    df["NumberSicCodes"] = 0
    for column in sic_cols:
        df["NumberSicCodes"] += df[column] != ""
    return df

def add_TotalAssetsLessLiabilities_calculated(df):
    def fn(df, year, assets_plus, assets_minus):
        df["TotalAssetsLessLiabilities_calculated.{}".format(year)] = 0
        for column in df.columns:
            if year in column:
                if column.replace(".{}".format(year), "") in assets_plus:
                    df = update_df_column_type(df, [column], "float64")
                    df["TotalAssetsLessLiabilities_calculated.{}"
                        .format(year)] = df["TotalAssetsLessLiabilities_calculated.{}".format(year)].add(df[column], fill_value=0)
                elif column.replace(".{}".format(year), "") in assets_minus:
                    df = update_df_column_type(df, [column], "float64")
                    df["TotalAssetsLessLiabilities_calculated.{}"
                        .format(year)] = df["TotalAssetsLessLiabilities_calculated.{}".format(year)].subtract(df[column], fill_value=0)
        return df

    assets_plus = ["ShareholderFunds", "FixedAssets", ":CurrentAssets", "CashBankOnHand", "ProvisionsForLiabilities",] #"FinancialAssets"
    assets_minus = [":Creditors", "TotalBorrowings"]

    df = fn(df, "yr1", assets_plus, assets_minus)
    df = fn(df, "yr0", assets_plus, assets_minus)
    return df
def remove_irrelevant_features(df):

    col_drop_policy = ["Insured Name", "Policy Number", "Division", "Company Number", "d:Equity.yr1"]
    col_drop_accounts = ["filename", "directory_name", "UKCompaniesHouseRegisteredNumber", "EntityCurrentLegalOrRegisteredName", "FinancialAssets"] #for all years
    drop_accounts_2 = []
    for value in col_drop_accounts:
        for key in df.keys():
            if value in key:
                drop_accounts_2.append(key)
    drop_labels = []
    drop_labels.extend(col_drop_policy)
    drop_labels.extend(drop_accounts_2)
    #drop_labels.extend(get_col_names_containing(["Sub Division", "Sub Portfolio", "Portfolio"], df))
    #drop_labels.extend(get_col_names_containing(["yr0"], df))
    return df.drop(labels=drop_labels, axis=1, inplace=False)

if __name__ == "__main__":
    run()
