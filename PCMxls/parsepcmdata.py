import pandas as pd
input_xlsx_path = "Copy of PCM Clients over £5K.xls.xlsx"
output_xlsx_path = "PCMinformationNanRemoved.xlsx"
def main():
    df = pd.read_excel(input_xlsx_path, sheet_name="InformationCopy", skipfooter=2, usecols="B:M")
    df = df.fillna(method="ffill")
    writer = pd.ExcelWriter(output_xlsx_path)
    df.to_excel(writer, "InformationParsed")
    writer.save()
    #result = df.groupby("Insured Name").size().sort_values()
    #print(result)


if __name__ == "__main__":
    main()
