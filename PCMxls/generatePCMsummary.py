import pandas as pd
input_xlsx_path = "PCMinformationNanRemoved.xlsx"
output_xlsx_path = "PCMinformationSummary.xlsx"
def main():
    df = pd.read_excel(input_xlsx_path, usecols="B:M")
    writer = pd.ExcelWriter(output_xlsx_path)
    result = df.groupby("Insured Name")["GWP"].sum().sort_values()
    print(result)
    #df.to_excel(writer, "InformationParsed")
    #writer.save()



if __name__ == "__main__":
    main()
