import pandas as pd
import requests
import sys
from io import BytesIO
import json
from DB_info import SQL_ARG
from sqlalchemy import create_engine, sql
from validation import validate_list_company_names, remove_company_stopwords
from features import numeric_variables, non_zero_variables, bool_variables, date_variables
from PCMdb import PCMdb as db
import pandas as pd
import numpy as np
import time
import datetime
from datetime import timedelta
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale


PCMdb = "QBE_policies_data"
PCM_comp_numbers = "QBE_company_numbers"
TABLE = PCMdb
QBE_2017_YEAR_END = datetime.datetime(2017, 12, 30)
DAYS_YEAR = 365


"""
80-20 split of data into train and test
X_train, y_train
X_test, y_test
"""
#Processing data
NUMBER_BINS_NUMERICAL = 5 #+1 for NaN

#Random Forest model parameters
n_estimators=300
min_samples_split=3
criterion="gini"
#criterion="mse"

n_estimators_options = [3**x for x in range(5)]
criteria = ["entropy", "gini"]
min_samples_splits = [2**2 for x in range(4)]


def main():
    engine = create_engine(SQL_ARG)
    with engine.connect() as conn:
        query = """SELECT *
                    FROM companies."QBE_policies_data_w_accounts_2yrs";"""
        df = pd.read_sql(query, conn)
    df = remove_irrelevant_features(df)
    #print(df)
    #df = scale(df) #- feature scaling. But am putting numerical values in bins
    df = preprocess_features(df)
    #print(df)
    col_remove_y = ["Claims", "Claim Count", "Banding"]
    X = df.drop(labels=col_remove_y, axis=1)
    y = df["Claims"] / df["GWP"] > 0.5
    y = parse_claim_GWP(y) #remove NaN and inf

    #print(list(weights))
    #y = df["Claims"] > 0

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    clf = RandomForestClassifier(n_estimators=n_estimators, min_samples_split=min_samples_split, criterion=criterion)

    weights = create_weightings(y_train.copy())
    #weights = weights.iloc[::-1]
    #weights = None
    clf.fit(X_train, y_train, sample_weight=weights)

    #print(clf.predict_proba(X_test))
    #sys.exit(2)

    xls_writer = pd.ExcelWriter("results/randomForest_important_features_claimsGWPratio2.xlsx")
    df = pd.DataFrame(clf.feature_importances_, X_train.keys())
    df.to_excel(xls_writer, "Sheet 1")
    xls_writer.save()


    train_score = clf.score(X_train, y_train)
    test_score = clf.score(X_test, y_test)
    result = {"n_estimators": n_estimators, "min_samples_split": min_samples_split,
                "criterion": criterion, "train_score": train_score, "test_score": test_score}


    print('Accuracy on training set: {:.2f}%'
         .format(clf.score(X_train, y_train)))
    print('Accuracy on test set: {:.2f}%'
         .format(clf.score(X_test, y_test)))
    pred = clf.predict(X_test)
    print(confusion_matrix(y_test, pred))
    print(classification_report(y_test, pred))


def create_weightings(y):
    counts = y.value_counts()
    weights = {}
    for key in counts.keys():
        weights[key] = 1/counts[key]*1000
    y.replace(weights, inplace=True)
    return y

def parse_claim_GWP(y):
    y.rename("Claim/Cost ratio", inplace=True)
    y.fillna(0, inplace=True)
    REPLACE_INF = 1
    y.replace(np.inf, REPLACE_INF, inplace=True)
    return y

def remove_irrelevant_features(df):

    col_drop_for_now = ["IncorporationDate"]
    col_drop_policy = ["Insured Name", "Policy Number", "Division", "Company Number"]
    col_drop_accounts = ["filename", "directory_name", "UKCompaniesHouseRegisteredNumber", "EntityCurrentLegalOrRegisteredName"] #for all years
    col_drop_accounts.extend(date_variables)
    drop_accounts_2 = []
    for value in col_drop_accounts:
        for key in df.keys():
            if value in key:
                drop_accounts_2.append(key)
    drop = []
    drop.extend(col_drop_policy)
    drop.extend(drop_accounts_2)
    drop.extend(col_drop_for_now)
    return df.drop(labels=drop, axis=1, inplace=False)

def preprocess_features(df):
    for variable in bool_variables:
        for column in df.keys():
            if variable in column:
                df[column].loc[pd.isnull(df[column])] = False

    df = preprocess_numeric_features_replace_missing(df)

    #replace missing values
    #put into bins
    #categoric features

    le=LabelEncoder()
    ohe=OneHotEncoder(sparse=False)
    for col in df.columns.values:
        # Encoding only categorical variables
        if df[col].dtypes=='object':
            le.fit(df[col])
            new_features = le.transform(df[col])
            new_features = new_features.reshape(-1,1)
            ohe.fit(new_features)
            temp = ohe.transform(new_features)
            temp=pd.DataFrame(temp, columns=[(col+"_"+str(i)) for i in df[col].value_counts().index])
            temp=temp.set_index(df.index.values)
            df = pd.concat([df,temp], axis=1)
            df = df.drop(labels=col, axis=1)
    return df

def preprocess_numeric_features(df):
    df = update_df_column_type(df, numeric_variables, "float64")
    num_var_col = get_col_names_containing(numeric_variables, df)
    for column in num_var_col:
        series = df[column]
        max = series.max()
        min = series.min()
        range_data = max - min
        frequency_not_null = series.count()
        class_width_cutoff = {}
        for class_index in range(NUMBER_BINS_NUMERICAL):
            percentage = class_index/NUMBER_BINS_NUMERICAL*100
            class_width_cutoff[class_index] = numpy.nanpercentile(series, percentage)
        def fn(x):
            pass
            return x
        series = series.map(fn)

        sys.exit(2)

        df[column].loc[pd.isnull(df[column]) == False] = "None" # set NaN to string
        #create dictionary with size of bins
        #alter each value and put in bin
        #change type to object (so it will be hot encoded)

    print(df[num_var_col])
    sys.exit(2)

    return df

def preprocess_numeric_features_replace_missing(df):
    variables = [x for x in numeric_variables if x not in non_zero_variables]
    df = update_df_column_type(df, numeric_variables, "float64")
    num_var_col = get_col_names_containing(numeric_variables, df)
    for column in num_var_col:
        df[column] = df[column].fillna(value=0)
    return df

def update_df_column_type(df, columns, new_type):
    for variable in columns:
        for column in df.keys():
            if variable in column:
                df[column] = df[column].astype(new_type)
    return df

def get_col_names_containing(variables_list, df):
    columns_final = []
    for variable in variables_list:
        min_samples_splits = [2**x for x in range(1,4)]
        columns_final.extend([x for x in df.keys() if variable in x])
    return columns_final

if __name__ == "__main__":
    main()
