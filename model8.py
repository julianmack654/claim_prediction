from model5 import run as model
from model5 import prepare_data
from model5 import parse_claim_GWP
import sys
import pandas as pd
import numpy as np



XLS_OUT = "results/profile_results1.xlsx"

def main():
    """
    Segment by company size
    """

    X, y, df_original = prepare_data(model="ratio", numerical_bins=False, save_data=True)

    df = pd.concat((X, y,), axis=1)

    df_micro = df[(df["AverageNumberEmployeesDuringPeriod.yr0"] < 10)]
    df_small = df[(df["AverageNumberEmployeesDuringPeriod.yr0"] < 50) & (df["AverageNumberEmployeesDuringPeriod.yr0"] >= 10)]
    df_medium = df[(df["AverageNumberEmployeesDuringPeriod.yr0"] >= 50) & (df["AverageNumberEmployeesDuringPeriod.yr0"] < 250)]
    df_large = df[(df["AverageNumberEmployeesDuringPeriod.yr0"] > 250)]

    company_dfs = [df_micro, df_small, df_medium, df_large]
    for df in company_dfs:
        y = df["Claim/Cost ratio"]
        X = df.drop(labels="Claim/Cost ratio", axis=1)
        clf, X_train, X_test, y_train, y_test, y_pred = model(X, y, random_state=5, save_features=True)

    
    print("Prediction that (claim size)/(GWP spent on policy) > 0.5 : ")
    clf, X_train, X_test, y_train, y_test, y_pred = model(X, y, random_state=5, save_features=True)
    print()
    print()






if __name__ == "__main__":
    main()
